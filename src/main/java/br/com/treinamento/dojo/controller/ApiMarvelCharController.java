package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiMarvelCharController {

	/*
	 * Retorna personagem por id
	 */
	@RequestMapping(value = "api/char/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> createUser(@PathVariable("id") long id) {
        System.out.println("Creating Char " + String.valueOf(id));
        return new ResponseEntity<String>(String.valueOf(id), HttpStatus.CREATED);
    }
	
	/*
	 * Retorna todos os personagens
	 */
	@RequestMapping(value = "api/char/", method = RequestMethod.GET)
    public ResponseEntity<List<String>> listAllUsers() {
        List<String> lista = new ArrayList<String>();
        lista.add("TESTE");
		return new ResponseEntity<List<String>>(lista, HttpStatus.OK);
    }
	
//	/*
//	 * Deletando o personagem
//	 */
	@RequestMapping(value = "api/char/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteChar(@PathVariable("id") long id) {
        System.out.println("Deletando personagem: " + id);
        return new ResponseEntity<String>(String.valueOf(id),HttpStatus.NO_CONTENT);
    }
	
	
}
