package br.com.treinamento.dojo.controller;



import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.treinamento.db.DB;
import br.com.treinamento.pojo.OutputJson;
import br.com.treinamento.pojo.Personagens;


@RestController
public class LoadController {

	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public ResponseEntity<String> helloWorld() {

		Client client = Client.create();

		String url = "http://gateway.marvel.com:80/v1/public/characters?ts=1&apikey=a86c3551887bf6c07aca8bb84cc697c9&hash=a0900b259aea3bc1dbf659a804c1a124";
		WebResource webResource = client.resource(url);

		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

		String output = response.getEntity(String.class);
		
		OutputJson outputJson = new Gson().fromJson(output, OutputJson.class);
		
		List<Personagens> personagens = outputJson.getPersonagens();
		
		for (Personagens personagem : personagens) {
			
			//DB.getInstance().saveCharacter(personagem.getId(), character);
			
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}

}
