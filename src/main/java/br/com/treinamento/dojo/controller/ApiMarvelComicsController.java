package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiMarvelComicsController {

	/*
	 * Retorna comic por id
	 */
	@RequestMapping(value = "api/comic/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> createUser(@PathVariable("id") long id) {
        System.out.println("Creating comic " + String.valueOf(id));
        return new ResponseEntity<String>(String.valueOf(id), HttpStatus.CREATED);
    }
	
	/*
	 * Retorna todos as comics
	 */
	@RequestMapping(value = "api/comic/", method = RequestMethod.GET)
    public ResponseEntity<List<String>> listAllUsers() {
        List<String> lista = new ArrayList<String>();
        lista.add("TESTE");
		return new ResponseEntity<List<String>>(lista, HttpStatus.OK);
    }
	
	/*
	 * Deletando a comic
	 */
	@RequestMapping(value = "/comic/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteChar(@PathVariable("id") long id) {
        System.out.println("Deletando comic: " + id);
        return new ResponseEntity<String>(String.valueOf(id),HttpStatus.NO_CONTENT);
    }
	
	
}
