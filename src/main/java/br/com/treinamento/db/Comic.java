package br.com.treinamento.db;

public class Comic {
	private int id;
	private int digitalId;
	private String title;
	private String resourceURI;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getDigitalId() {
		return digitalId;
	}
	
	public void setDigitalId(int digitalId) {
		this.digitalId = digitalId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getResourceURI() {
		return resourceURI;
	}
	
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	
}
