package br.com.treinamento.db;

import java.util.List;

public class Character {
	
	private int id;
	private String name;
	private String description;
	private List<Comic> comicsList;
	
	public List<Comic> getComicsList() {
		return comicsList;
	}

	public void setComicsList(List<Comic> comicsList) {
		this.comicsList = comicsList;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}
