package br.com.treinamento.db;

import java.util.ArrayList;
import java.util.HashMap;

public class DB {

	private static DB instance = null;
	private static HashMap<Integer, Character> characterMap;
	
	private DB(){
		characterMap = new HashMap<Integer, Character>();
	}
	
	public static DB getInstance(){
		if(instance == null){
			instance = new DB();
		}
		
		return instance;
	}
	
	public static void deleteCharacter(Integer id){
		if(characterMap.containsKey(id)){
			characterMap.remove(id);			
		}
	}
	
	public static Character getCharacter(Integer id){
		return characterMap.get(id);
	}

	public static void saveCharacter(Integer id, Character character){
		if(characterMap.containsKey(id)){
			characterMap.remove(id);
		}
		
		characterMap.put(id, character);			
	}
	
	
	
}
