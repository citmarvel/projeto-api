package br.com.treinamento.pojo;

import java.util.Arrays;
import java.util.List;

public class OutputJson {

	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	
	public List<Personagens> getPersonagens(){
		return  Arrays.asList(getData().getResults());
	}
	
}

class Data {
	private Personagens[] results;

	public Personagens[] getResults() {
		return results;
	}

	public void setResults(Personagens[] results) {
		this.results = results;
	}


	
}

